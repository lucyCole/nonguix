;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 peridot lazuli <coleclough.lucy@gmail.com>

(define-module (nongnu home services editors)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu home services)
  #:use-module (guix modules)
  #:use-module (json)
  #:use-module (nongnu packages editors)

  #:export (<home-vscodium-configuration> home-vscodium-configuration
                                     make-home-vscodium-configuration
                                     home-vscodium-configuration?
                                     this-home-vscodium-configuration
                                     home-vscodium-configuration-snippets
                                     home-vscodium-configuration-keybindings
                                     home-vscodium-configuration-settings
                                     home-vscodium-configuration-vsix-extensions

                                     home-vscodium-service-type))

(define-record-type* <home-vscodium-configuration> home-vscodium-configuration
                     make-home-vscodium-configuration
  home-vscodium-configuration?
  this-home-vscodium-configuration
  (snippets home-vscodium-configuration-snippets
            (default (list)))
  (keybindings home-vscodium-configuration-keybindings
               (default (vector)))
  (settings home-vscodium-configuration-settings
            (default (list)))
  (vsix-extensions home-vscodium-configuration-vsix-extensions
                   (default (list))))

(define home-vscodium-service-type
  (service-type (name 'vscodium)
                (extensions (list (service-extension
                                   home-xdg-configuration-files-service-type
                                   (lambda (config)
                                     ;; Install { keybind-(*), setting-(*), snippet.(*)} to $XDG_CONFIG_HOME/VSCodium/User/*
                                     (let ((base "VSCodium/User/"))
                                       `((,(string-append base "keybinds.json") ,
                                          (plain-file "vscodium-keybindings"
                                                      (scm->json-string (home-vscodium-configuration-keybindings
                                                                         config))))
                                         (,(string-append base "settings.json") ,
                                          (plain-file "vscodium-settings"
                                                      (scm->json-string (home-vscodium-configuration-settings
                                                                         config))))
                                         ,@(map (lambda (snippetEntry)
                                                  (let ((name (car
                                                               snippetEntry))
                                                        (data (cdr
                                                               snippetEntry)))
                                                    (list (string-append base
                                                           "snippets/" name
                                                           ".code-snippets")
                                                          (plain-file (string-append
                                                                       "vscodium-snippet-"
                                                                       name)
                                                                      (scm->json-string
                                                                       data)))))
                                                (home-vscodium-configuration-snippets
                                                 config))))))
                                  (service-extension
                                   home-activation-service-type
                                   (lambda (config)
                                     ;; install extension-(*) upon re-configure using `codium --install-extension /gnu/store/<extensionPath> --force`
                                     ;;
                                     ;; a better option i think would be to un-pack each vsix file using `unzip` in a computed-file and then storing those item-(*) in the store
                                     ;; then referencing those from $HOME/.vscode-oss/extensions/extensions.json
                                     ;; the general structure i had going was: ( in the home-files-service-type extension)
                                     ;; map each extension to a list which is passed to scm->json-string to create the extensions.json file
                                     ;; in the mapping function:
                                     ;; create the compute-ed-file which unzip-s the vsix into a store location
                                     ;; contain that in a gexp and pass that to gexp->derivation which is run through the store to build the file
                                     ;; use the reultant store name as well as the original name to create the alist entry for extensions.json
                                     
                                     #~(begin
                                         (define extensionPaths
                                           (list #$@(home-vscodium-configuration-vsix-extensions
                                                     config)))
                                         (for-each (lambda (extensionPath)
                                                     (system* (string-append #$vscodium
                                                               "/bin/codium")
                                                      "--install-extension"
                                                      extensionPath "--force"))
                                                   extensionPaths))))
                                  (service-extension home-profile-service-type
                                   (const (list vscodium)))))
                (default-value (home-vscodium-configuration))
                (description
                 "install vscodium using specified value-(*) for:
snippet-(*)
setting-(*)
keybind-ing-(*)
extent-tion-(*)

to do this, one can use a <vscode-configuration> record:
snippets
	a list of pair-(*) where the car is the name of the snippet and the cdr is an assoc list representing the snippet configuration
keybindings
	a vector of keybinding object-(*) in the vscodium \"keybindings.json\" format
settings
	a association list in the vscodium \"settings.json\" format
extensions
	a list of file like object-(*) which resolve to a vsix extension file
	
any {
	snippet pair cdr,
	keybinding,
	setting,
}
must be in a format acceptable by the guile-json module, in other word-(*) vector-(*) must be used, not list-(*)")))
